## Research compendium for...

### Compendium DOI:

[![DOI](https://img.shields.io/badge/DOI-10.17605/252FOSF.IO/4MDSK-blue)](https://doi.org/10.17605/OSF.IO/4MDSK)

### Author of this repository:

[![ORCiD](https://img.shields.io/badge/ORCiD-0000--0003--0655--7603-green.svg)](http://orcid.org/0000-0003-0655-7603) Mirco Brunner

### Published in:

### Abstract:

### Keywords:

### Overview of contents:

### How to reproduce:

### Licenses:

Creative Commons Attribution 4.0 International Public License

copyright holder: My Name, year: 2022

Code: MIT <http://opensource.org/licenses/MIT>

Data: Please see the license agreements of XXX and XXX

Text: Please see the license agreements of the XXX journal
